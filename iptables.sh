#!/bin/bash

# This has to be ran by root to redirect DNS packets from port 16333 <--> 53
# so that you can avoid running wayblock as privileged users

if [[ ! $(/sbin/iptables -L -t nat | grep 16333) ]]
then
  /sbin/iptables -t nat -A PREROUTING -p udp --dport 53 -j DNAT --to-destination :16333
  /sbin/iptables -A INPUT -p udp --dport 16333 -j ACCEPT
  /sbin/iptables -A INPUT -p udp --dport 53 -j ACCEPT
fi

if [[ ! $(/sbin/ip6tables -L -t nat | grep 16333) ]]
then
  /sbin/ip6tables -t nat -A PREROUTING -p udp --dport 53 -j DNAT --to-destination :16333
  /sbin/ip6tables -A INPUT -p udp --dport 16333 -j ACCEPT
  /sbin/ip6tables -A INPUT -p udp --dport 53 -j ACCEPT
fi
