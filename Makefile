
all:
	g++ -std=c++11 -Wall -o wayblock \
	$(shell curl-config --cflags) \
	src/*.cpp \
	$(shell curl-config --libs)

clean:
	rm wayblock
