
# wayblock

`wayblock` is a DNS server that looks up records in the
[Wayback Machine](https://archive.org/web/) and blocks all queries for records
newer than some specified time period, which is 2 years by default. `wayblock`
itself doesn't handle DNS resolution but instead passes the requests to an
upstream server and is designed to be used with existing DNS infrastructure

## On Demand Caching

TODO explain caching.

## Building

Grab code, make code, install code.

```
git clone https://gitlab.com/krisives/wayblock.git
cd wayblock
sudo apt install build-essential g++ libcurl4-gnutls-dev
make
```

## Options

`wayblock` has the following command-line options:

| Flag            | Meaning                                               |
|-----------------|-------------------------------------------------------|
| -p *port*       | Listen on port number (default: 16333)                |
| -u *ip*         | Set upstream DNS resolver (default: 8.8.8.8)          |
| -t *yymmdd*     | Block requests newer than (default: 2 years ago)      |
| -n              | Disable caching (default: enabled)                    |
| -c              | Set cache path (default: .cache)                      |
| -v              | Verbose output (default: disabled)                    |

## License

`wayblock` is dual licensed under the MIT or The Derbycon License.
