
#include <iostream>
#include "Server.hpp"
#include "Packet.hpp"
#include "Util.hpp"
#include "json.hpp"

Server::Server() {
  bindAddress.sin6_family = AF_INET6;
  bindAddress.sin6_addr = in6addr_any;
  bindAddress.sin6_port = htons(16333);
  realDnsAddr.sin_family = AF_INET;
  realDnsAddr.sin_port = htons(53);
  realDnsAddr.sin_addr.s_addr = inet_addr("8.8.8.8");
}

Server::~Server() {
  closeSocket();
}

void Server::stop() {
  stopped = true;
}

bool Server::isRunning() {
  return !stopped;
}

void Server::closeSocket() {
  if (sock) {
    verbose("Closing socket");

    if (close(sock) < 0) {
      throw std::runtime_error("close() for socket failed");
    }

    sock = 0;
  }
}

void Server::listen() {
  if (sock) {
    throw std::runtime_error("Socket already exists");
  }

  FD_ZERO(&readFileDescs);
  sock = socket(AF_INET6, SOCK_DGRAM, IPPROTO_UDP);

  if (sock <= 0) {
    throw std::runtime_error("Unable to open inbound socket");
  }

  int ok = bind(sock, (struct sockaddr*)&bindAddress, sizeof(bindAddress));

  if (ok < 0) {
    throw std::runtime_error("Unable to bind inbound socket");
  }

  FD_SET(sock, &readFileDescs);
}

void Server::tick() {
  while (isRunning()) {
    tickSelect();
  }
}

void Server::tickSelect() {
  fd_set active = readFileDescs;
  int maxFileDesc = sock;

  for (auto client : clients) {
    maxFileDesc = std::max(maxFileDesc, client.outboundSocket);
  }

  int activity = select(maxFileDesc + 1, &active, NULL, NULL, NULL);

  if (activity < 0) {
    throw std::runtime_error("Unable to select() sockets");
  }

  if (FD_ISSET(sock, &active)) {
    tickListener();
  }

  for (auto it = clients.begin(); it != clients.end(); ) {
    Client& client = *it;

    if (FD_ISSET(client.outboundSocket, &active)) {
      try {
        client.recvAnswer(*this);
      } catch (std::exception& e) {
        verbose("Error receiving answer " + (std::string)e.what());
        client.closeSocket();
      }
    } else {
      client.timeout();
    }

    if (!client.outboundSocket) {
      if (client.originalSocket) { FD_CLR(client.originalSocket, &readFileDescs); }
      it = clients.erase(it);
    } else {
      ++it;
    }
  }
}

void Server::tickListener() {
  while (isRunning()) {
    try {
      if (!recvQuestion()) {
        return;
      }
    } catch (std::exception& e) {
      std::cerr << "Error while receiving a DNS question:" << e.what() << std::endl;
      break;
    }
  }
}

bool Server::recvQuestion() {
  char controlData[CMSG_SPACE(sizeof(struct in6_pktinfo))];
  char recvBuffer[1024];
  struct sockaddr_in6 senderAddress;
  struct msghdr msg;
  //struct in6_pktinfo *packetInfo = NULL;
  //struct cmsghdr *cmsg = NULL;

  Util::zero(&controlData, sizeof(controlData));
  Util::zero(&recvBuffer, sizeof(recvBuffer));
  Util::zero(&senderAddress, sizeof(senderAddress));
  Util::zero(&msg, sizeof(msg));

  // Prepare to receive a packet
  msg.msg_name = (struct sockaddr*)&senderAddress;
  msg.msg_namelen = sizeof(senderAddress);
  struct iovec iov[1];
  iov[0].iov_base = &recvBuffer;
  iov[0].iov_len = sizeof(recvBuffer);
  msg.msg_iov = iov;
  msg.msg_iovlen = 1;
  msg.msg_control = &controlData;
  msg.msg_controllen = sizeof(controlData);

  // Actually receive a packet
  int recvSize = recvmsg(sock, &msg, MSG_DONTWAIT);

  if (recvSize == 0) {
    // Skip empty packets entirely
    return false;
  } else if (recvSize == -1) {
    if (errno == EAGAIN || errno == EWOULDBLOCK) {
      // Skip non-blocking errors
      return false;
    }

    throw std::runtime_error("Unable to recvmsg()");
  }

  // Parse control data to find IPV6_PKTINFO
  // for (cmsg = CMSG_FIRSTHDR(&msg); cmsg != NULL; cmsg = CMSG_NXTHDR(&msg, cmsg)) {
  //   if (cmsg->cmsg_level == IPPROTO_IPV6 && cmsg->cmsg_type == IPV6_PKTINFO) {
  //     packetInfo = (struct in6_pktinfo*) CMSG_DATA(cmsg);
  //     break;
  //   }
  // }

  // if (!packetInfo) {
  //   throw std::runtime_error("Cannot determine destination IP address");
  // }

  Packet packet(recvBuffer, recvSize);

  if (!codec.decode(packet)) {
    throw std::runtime_error("Skipping a DNS packet");
  }

  if (codec.isResponse()) {
    throw std::runtime_error("Skipping DNS packet on inbound socket because isResponse");
  }

  verbose("Question:");

  for (auto& question : codec.questions) {
    verbose("  " + question.domain);
  }

  recvQuestion(packet, senderAddress);
  return true;
}

void Server::recvQuestion(
  Packet& p,
  struct sockaddr_in6& senderAddress
) {
  Client client;
  client.id = codec.header.id;
  client.addr = senderAddress;

  for (auto &question : codec.questions) {
    if (!checkDomain(question.domain)) {
      sendBlockResponse(client);
      return;
    }
  }

  client.outboundSocket = createOutboundSocket();
  client.originalSocket = client.outboundSocket;
  clients.push_back(client);
  FD_SET(client.outboundSocket, &readFileDescs);
  forwardRequest(client, p);
}

bool Server::checkDomain(const std::string domain) {
  if (enableCaching) {
    int cacheHit = cache.lookup(domain);

    if (cacheHit != DomainCache::RESULT_NONE) {
      verbose("Cache hit for " + domain);
      return cacheHit == DomainCache::RESULT_ALLOW;
    }
  }

  bool allow = false;

  for (auto plugin : plugins) {
    allow = plugin->checkDomain(domain);

    if (!allow) {
      verbose("Blocked by " + plugin->name);
      break;
    }
  }

  if (enableCaching) {
    cache.update(domain, allow);
  }

  return allow;
}

void Server::sendBlockResponse(Client& client) {
  codec.header = {};
  codec.header.id = client.id;
  codec.header.flags = Codec::FLAG_RESPONSE | Codec::FLAG_RECURSION_AVAILABLE;

  codec.additional.clear();
  codec.auths.clear();
  codec.answers.clear();

  for (auto question : codec.questions) {
    DnsResource answer = {};
    answer.domain = question.domain;
    // TODO allow configure of TTL for blocks
    answer.header.ttl = 15;
    answer.header.rtype = 1;
    answer.header.rclass = 1;
    answer.header.dataLen = sizeof(in_addr);

    struct in_addr addr4;
    //inet_pton(AF_INET, ip.c_str(), &addr4);
    inet_pton(AF_INET, "127.0.0.1", &addr4);
    answer.raw.resize(sizeof(addr4));
    Util::copy(answer.raw.data(), &addr4, sizeof(addr4));

    //if (client.ipv4) {

    // } else {
    //   struct in6_addr addr6;
    //   inet_pton(AF_INET6, ip.c_str(), &addr6);
    //   answer.raw.resize(sizeof(addr6));
    //   memcpy(answer.raw.data(), &addr6, sizeof(addr6));
    // }

    codec.answers.push_back(answer);
  }

  Packet packet(1024);

  if (!codec.encode(packet)) {
    throw std::runtime_error("Failed to encode a fake response packet");
  }

  sendtofrom(sock, packet.raw.data(), packet.raw.size(), client.addr);
}

void Server::sendRedirectResponse(Client& client, const std::string redirect) {
  codec.header = {};
  codec.header.id = client.id;
  codec.header.flags = Codec::FLAG_RESPONSE | Codec::FLAG_RECURSION_AVAILABLE;

  codec.additional.clear();
  codec.auths.clear();
  codec.answers.clear();

  for (auto question : codec.questions) {
    DnsResource answer = {};
    answer.domain = question.domain;
    answer.header.ttl = 15;
    answer.header.rtype = 5; // CNAME
    answer.header.rclass = 1;
    answer.raw = Packet::encodeString(redirect);
    answer.header.dataLen = answer.raw.size();
    codec.answers.push_back(answer);
  }

  Packet packet(1024);

  if (!codec.encode(packet)) {
    throw std::runtime_error("Failed to encode a redirect packet");
  }

  sendtofrom(sock, packet.raw.data(), packet.raw.size(), client.addr);
}

void Server::forwardRequest(Client& client, Packet& packet) {
  int ok = sendto(
    client.outboundSocket,
    packet.raw.data(), packet.raw.size(),
    MSG_DONTWAIT,
    (sockaddr*)&realDnsAddr, sizeof(realDnsAddr)
  );

  if (ok < 0) {
    throw std::runtime_error("Unable to forward DNS request");
  }
}

int Server::createOutboundSocket() {
  struct sockaddr_in addr;
  addr.sin_family = AF_INET;
  addr.sin_port = htons(14223 + (rand() & 0xfff));
  addr.sin_addr.s_addr = INADDR_ANY;
  int outboundSocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

  if (outboundSocket <= 0) {
    throw std::runtime_error("Unable to open outbound socket");
  }

  int ok = bind(outboundSocket, (struct sockaddr*)&addr, sizeof(addr));

  if (ok < 0) {
    throw std::runtime_error("Unable to bind outbound socket");
  }

  return outboundSocket;
}

void Server::sendResponse(Client& client, Packet& packet) {
  if (!sock) {
    throw std::runtime_error("Not sending a packet because socket is gone");
  }

  sendtofrom(sock, packet.raw.data(), packet.raw.size(), client.addr);
}

void Server::sendtofrom(int s, const char *buffer, unsigned int bufferSize, struct sockaddr_in6& to) {
  if (s <= 0) {
    throw std::runtime_error("Missing socket");
  }

  int ok = sendto(
    s,
    buffer,
    bufferSize,
    MSG_DONTWAIT,
    (sockaddr*)&to,
    sizeof(to)
  );

  if (ok < 0) {
    if (errno != EAGAIN && errno != EWOULDBLOCK) {
      throw std::runtime_error("Unable to sendmsg()");
    }
  }
}

void Server::verbose(const std::string str) {
  if (enableVerbose) {
    std::cerr << str << std::endl;
  }
}
