
#pragma once

#include <string>
#include "Server.hpp"

class App {
private:
  Server server;
public:
  App();
  ~App();
  void setOptions(const int argc, char* const argv[]);
  bool configure(char option, std::string value);
  void run();
  void tick();
  void stop();
  bool isRunning();
};
