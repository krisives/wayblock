
#pragma once

#include <sys/socket.h>
#include <list>
#include <memory>

#include "Client.hpp"
#include "Codec.hpp"
#include "Plugin.hpp"
#include "DomainCache.hpp"

class Client;
class Packet;

class Server {
public:
  bool stopped = false;
  std::list<Client> clients;
  std::list<std::shared_ptr<Plugin>> plugins;
  int sock = 0;
  fd_set readFileDescs;
  sockaddr_in6 bindAddress;
  Codec codec;
  sockaddr_in realDnsAddr;
  DomainCache cache;
  bool enableCaching = true;
  bool enableVerbose = false;

  Server();
  ~Server();
  void stop();
  bool isRunning();
  void closeSocket();
  void listen();
  void tick();
  void tickSelect();
  void tickListener();
  bool recvQuestion();
  void recvQuestion(Packet& packet, struct sockaddr_in6& senderAddress);
  void sendResponse(Client& client, Packet& packet);
  void forwardRequest(Client& client, Packet& packet);
  //void sendFakeResponse(Client& client);
  //void sendFakeResponse(Client& client, const std::string ip);
  bool checkDomain(const std::string domain);
  void sendBlockResponse(Client& client);
  void sendRedirectResponse(Client& client, const std::string redirect);
  bool isAccessEnabled(Client& client);
  int createOutboundSocket();
  void removeSocket(int outboundSocket);
  void verbose(const std::string str);

  void sendtofrom(int s, const char *buffer, unsigned int bufferSize, struct sockaddr_in6& to);
};
