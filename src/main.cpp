
#include <iostream>
#include <stdexcept>
#include <signal.h>

#include "App.hpp"

static class App app;

void handleSignal(int signal) {
  app.stop();
}

int main(const int argc, char* const argv[]) {
  signal(SIGINT, handleSignal);
  app.setOptions(argc, argv);

  try {
    app.run();
  } catch (std::exception &e) {
    std::cerr << "ERROR: " << e.what() << std::endl;
    return 1;
  }

  return 0;
}
