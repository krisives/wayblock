
#pragma once

#include <string>
#include <vector>

struct DnsHeader {
  unsigned short id;
  unsigned short flags;
  unsigned short qdcount; // Question Section
  unsigned short ancount; // Answer Section
  unsigned short nscount; // Authority Section
  unsigned short arcount; // Additional Section
} __attribute__ ((packed));

struct DnsResourceHeader {
  unsigned short rtype;
  unsigned short rclass;
  unsigned int ttl;
  unsigned short dataLen;
} __attribute__ ((packed));

struct DnsResource {
  std::string domain;
  struct DnsResourceHeader header;
  std::vector<char> raw;
};
