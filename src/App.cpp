
#include <stdexcept>
#include "App.hpp"
#include "WaybackPlugin.hpp"
#include "WhoisPlugin.hpp"

App::App() {
  server.plugins.push_back(std::make_shared<WaybackPlugin>());
  server.plugins.push_back(std::make_shared<WhoisPlugin>());
}

App::~App() {

}

void App::run() {
  server.listen();

  if (server.enableCaching) {
    server.cache.init();
  }

  while (isRunning()) {
    tick();
  }
}

void App::tick() {
  server.tick();
}

void App::stop() {
  server.stop();
}

bool App::isRunning() {
  return server.isRunning();
}

void App::setOptions(int argc, char* const argv[]) {
  std::string opts = "p:u:nc:v";
  int option = 0;

  for (auto plugin : server.plugins) {
    opts += plugin->options;
  }

  while ((option = getopt(argc, argv, opts.c_str())) != -1) {
    const std::string value = optarg ? std::string(optarg) : "";
    bool valid = configure(option, value);

    for (auto plugin : server.plugins) {
      valid |= plugin->configure(option, value);
    }

    if (!valid) {
      throw std::runtime_error("Unknown command line option");
    }
  }
}

bool App::configure(char option, const std::string value) {
  switch (option) {
  case 'p':
    // port number
    server.bindAddress.sin6_port = htons(std::stoi(value));
    break;
  case 'u':
    server.realDnsAddr.sin_addr.s_addr = inet_addr(value.c_str());
    break;
  case 'n':
    server.enableCaching = false;
    break;
  case 'c':
    server.cache.path = value;
    break;
  case 'v':
    server.enableVerbose = true;
    break;
  default:
    return false;
  }

  return true;
}
