
#include <iostream>
#include <fstream>
#include <stdexcept>
#include <sys/stat.h>
#include <time.h>

#include "DomainCache.hpp"

// TODO some of this can use C++17 std::filesystem

void DomainCache::init() {
  int result = mkdir(path.c_str(), 0755);

  if (result != 0 && errno != EEXIST) {
    throw std::runtime_error("Cannot create cache directory");
  }
}

int DomainCache::lookup(const std::string host) {
  const std::string entryPath = path + "/" + host;
  struct stat fstats = {};

  if (stat(entryPath.c_str(), &fstats) != 0) {
    return RESULT_NONE;
  }

  std::ifstream fs(entryPath);

  if (!fs.good()) {
    return RESULT_NONE;
  }

  time_t now = time(NULL);
  time_t age = now - fstats.st_mtime;

  if (age >= maxCacheTime) {
    return RESULT_NONE;
  }

  bool result = false;
  fs >> result;
  return result ? RESULT_ALLOW : RESULT_BLOCK;
}

void DomainCache::update(const std::string host, bool allowed) {
  const std::string entryPath = path + "/" + host;
  std::ofstream fs(entryPath);
  fs << allowed;
}
