
#pragma once

#include "Plugin.hpp"

class WhoisPlugin : public Plugin {
public:
  WhoisPlugin();
  ~WhoisPlugin();
  bool configure(char option, const std::string value) override;
  bool checkDomain(const std::string host) override;
};
