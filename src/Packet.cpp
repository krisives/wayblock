
#include <cstring>
#include <arpa/inet.h>
#include "Util.hpp"
#include "Packet.hpp"

Packet::Packet(std::vector<char> v) : raw(v) {
  pos = 0;
}

Packet::Packet(unsigned int size) : raw(size) {
  pos = 0;
}

Packet::Packet(char *buffer, unsigned int len) : raw(buffer, buffer + len) {
  pos = 0;
}

unsigned char Packet::readByte() {
  return raw.at(pos++);
}

void Packet::writeByte(unsigned char c) {
  raw.at(pos++) = c;
}

void Packet::readData(void* buffer, unsigned int len) {
  if ((pos + len) > raw.size()) {
    throw std::runtime_error("Attempt to read beyond packet bounds");
  }

  std::memcpy(buffer, raw.data() + pos, len);
  pos += len;
}

void Packet::writeData(const void *buffer, unsigned int len) {
  if ((pos + len) > raw.size()) {
    throw std::runtime_error("Attempt to write beyond packet bounds");
  }

  std::memcpy(raw.data() + pos, buffer, len);
  pos += len;
}

std::string Packet::readString() {
  unsigned char c;
  std::string str;

  do {
    c = readByte();
    if (c && !str.empty()) { str += '.'; }

    if (c & 0b11000000) {
      str += readStringCompressed(c);
      c = 0;
    } else if (c) {
      str += readStringLiteral(c);
    }
  } while (c);

  return str;
}

std::string Packet::readStringLiteral(unsigned char len) {
  if (len == 0) {
    return "";
  } else if (len > 63) {
    throw std::runtime_error("DNS literal strings cannot be longer than 63 bytes");
  }

  char chunk[64];
  readData(chunk, len);
  chunk[len] = '\0';
  return chunk;
}

std::string Packet::readStringCompressed(unsigned char c) {
  unsigned short offset = c;
  if ((offset & 0b11000000) != 0b11000000) { throw std::runtime_error("DNS labels should have 2 most significant bits set"); }
  offset = ntohs((offset & 0b00111111) | (readByte() << 8));
  if (offset > pos) { throw std::runtime_error("DNS labels must point backwards"); }
  return peekString(offset);
}

std::string Packet::peekString(unsigned int offset) {
  unsigned int t = pos;
  pos = offset;
  std::string s = readString();
  pos = t;
  return s;
}

void Packet::writeString(std::string str) {
  if (str.empty()) {
    writeByte(0);
    return;
  }

  auto exists = lookup.find(str);

  if (exists != lookup.end()) {
    unsigned short offset = 0b11000000 | htons(exists->second);
    writeByte(offset & 0xFF);
    writeByte(offset >> 8);
    return;
  }

  lookup[str] = pos;
  auto parts = Util::split(str, '.');
  for (auto str : parts) { writeStringLiteral(str); }
  writeByte('\0');
}

void Packet::writeStringLiteral(std::string s) {
  if (s.size() > 63) { throw std::runtime_error("DNS strings cannot be longer than 63 chars"); }
  writeByte(s.size());
  writeData(s.c_str(), s.size());
}

std::vector<char> Packet::encodeString(std::string s) {
  Packet p(512);
  p.writeString(s);
  p.raw.resize(p.pos);
  return p.raw;
}

std::string Packet::decodeString(std::vector<char> v) {
  return Packet(v).readString();
}

std::vector<char> Packet::compress(std::vector<char> v, int relative) {
  std::string s = decodeString(v);
  if (s.empty()) { return v; }
  auto exists = lookup.find(s);

  if (exists == lookup.end()) {
    lookup[s] = pos + relative;
    return v;
  }

  return encodeShort(0b11000000 | htons(exists->second));
}

std::vector<char> Packet::encodeShort(unsigned short x) {
  std::vector<char> v(2);
  v[0] = x & 0xFF;
  v[1] = x >> 8;
  return v;
}
