
#include <cstdio>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>
#include <array>
#include "Util.hpp"

std::vector<std::string> Util::split(const std::string& s, char delim) {
  std::vector<std::string> elems;
  std::stringstream ss(s);
  std::string item;

  while (std::getline(ss, item, delim)) {
    elems.push_back(item);
  }

  return elems;
}

void Util::zero(void *buffer, const size_t length) {
  std::memset(buffer, 0, length);
}

void Util::copy(void *dest, const void *source, const size_t length) {
  std::memcpy(dest, source, length);
}

std::string Util::command(const std::string command) {
  std::array<char, 128> buffer;
  std::string result;
  std::shared_ptr<FILE> pipe(popen(command.c_str(), "r"), pclose);

  if (!pipe) {
    throw std::runtime_error("popen() failed!");
  }

  while (!feof(pipe.get())) {
    if (fgets(buffer.data(), 128, pipe.get()) != nullptr) {
      result += buffer.data();
    }
  }

  return result;
}
