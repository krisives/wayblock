
#pragma once

#include <string>

class Plugin {
public:
  std::string options;
  std::string name;

  Plugin(const std::string name);
  ~Plugin();

  /** Parse a configuration option from the command line */
  virtual bool configure(char option, const std::string value) = 0;

  /** Check if a domain should be allowed */
  virtual bool checkDomain(const std::string host) = 0;
};
