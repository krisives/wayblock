
#pragma once

#include <vector>
#include <string>
#include <sstream>
#include <cstring>

class Util {
public:
  static std::vector<std::string> split(const std::string& s, char delim);
  static void zero(void *buffer, const size_t len);
  static void copy(void *dest, const void *source, const size_t length);
  static std::string command(const std::string command);
};
