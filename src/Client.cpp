
#include <stdexcept>
#include <time.h>
#include "Client.hpp"
#include "Server.hpp"
#include "Packet.hpp"

Client::Client() {
  startTime = time(NULL);
}

Client::~Client() {
  // Ensure socket is closed (add this once working and debug to ensure working correctly)
}

void Client::closeSocket() {
  if (outboundSocket) {
    if (close(outboundSocket) < 0) {
      throw std::runtime_error("Unable to close() client socket");
    }

    outboundSocket = 0;
  }
}

void Client::timeout() {
  int age = time(NULL) - startTime;

  if (age > 20) {
    closeSocket();
  }
}

void Client::recvAnswer(Server& server) {
  char recvBuffer[1024];

  int recvSize = recv(outboundSocket, recvBuffer, sizeof(recvBuffer), MSG_DONTWAIT);

  if (recvSize < 0) {
    throw std::runtime_error("Error trying to recv()");
  }

  closeSocket();

  if (recvSize == 0) {
    throw std::runtime_error("Upstream DNS server sent us an empty packet");
  }

  Packet packet(recvBuffer, recvSize);

  if (!server.codec.decode(packet)) {
    throw std::runtime_error("Upstream DNS server sent us a packet we cannot decode");
  }

  // TODO move to Server make Client a plain object
  // TODO allow custom TTL response
  // for (auto& answer : server.codec.answers) {
  //   // if (answer.header.rtype == 1) {
  //   //   in_addr_t *answerIp = (in_addr_t*)answer.raw.data();
  //   //
  //   //   // for (auto ip : server.hide) {
  //   //   //   if (ip == *answerIp) {
  //   //   //     server.sendFakeResponse(*this);
  //   //   //     return;
  //   //   //   }
  //   //   // }
  //   // }
  //
  //   answer.header.ttl = 1;
  // }

  packet = Packet(1024);

  if (!server.codec.encode(packet)) {
    throw std::runtime_error("Upstream DNS server sent us a packet we cannot encode");
  }

  server.sendResponse(*this, packet);
}
