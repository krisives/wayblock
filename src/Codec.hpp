
#pragma once

#include <list>
#include "Dns.hpp"

class Packet;

class Codec {
public:
  DnsHeader header;
  std::list<DnsResource> questions;
  std::list<DnsResource> answers;
  std::list<DnsResource> auths;
  std::list<DnsResource> additional;

  bool decode(Packet& p);
  bool encode(Packet& p);

  DnsResource readQuestion(Packet& p);
  DnsResource readResource(Packet& p);
  void writeQuestion(Packet& p, DnsResource& res);
  void writeResource(Packet& p, DnsResource& res);

  bool isResponse() { return header.flags & FLAG_RESPONSE; }
  bool isAuthAnswer() { return header.flags & FLAG_AUTH_ANSWER; }
  bool isTruncated() { return header.flags & FLAG_TRUNCATED; }
  bool isRecursionDesired() { return header.flags & FLAG_RECURSION_DESIRED; }
  bool isRecursionAvail() { return header.flags & FLAG_RECURSION_AVAILABLE; }

  static const unsigned short FLAG_RESPONSE = 1 << 15;
  static const unsigned short FLAG_AUTH_ANSWER = 1 << 10;
  static const unsigned short FLAG_TRUNCATED = 1 << 9;
  static const unsigned short FLAG_RECURSION_DESIRED = (1 << 8);
  static const unsigned short FLAG_RECURSION_AVAILABLE = (1 << 7);
};
