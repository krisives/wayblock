
#pragma once

#include <string>
#include "SocketLibs.hpp"

class Server;

class Client {
public:
  struct sockaddr_in6 addr;
  //struct in6_addr listenAddress;
  //std::string local, remote;
  //bool ipv4 = false;
  int outboundSocket = 0;
  int originalSocket = 0;
  int startTime = 0;
  unsigned short id = 0;

  Client();
  ~Client();
  void closeSocket();
  void recvAnswer(Server& server);
  void timeout();
};
