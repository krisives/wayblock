
#pragma once

#include <string>

class DomainCache {
public:
  std::string path = ".cache";
  int maxCacheTime = 60 * 60 * 24 * 7;

  void init();
  int lookup(const std::string host);
  void update(const std::string host, bool allowed);

  const static int RESULT_NONE = 0;
  const static int RESULT_ALLOW = 1;
  const static int RESULT_BLOCK = -1;
};
