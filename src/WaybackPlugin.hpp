
#pragma once

#include "Plugin.hpp"
#include <curl/curl.h>
#include <time.h>

class WaybackPlugin : public Plugin {
public:
  CURL *curl = nullptr;
  time_t cutoff = 0;
  std::string response;

  WaybackPlugin();
  ~WaybackPlugin();
  bool configure(char option, const std::string value) override;
  bool checkDomain(const std::string domain) override;
  time_t parseTime(const std::string timestamp) const;
};
