
#include <stdexcept>
#include <getopt.h>
#include "WaybackPlugin.hpp"
#include "json.hpp"

#include <iostream>

WaybackPlugin::WaybackPlugin() : Plugin("wayback") {
  options = "t:";
  curl = curl_easy_init();
  cutoff = time(NULL) - (60 * 60 * 24 * 365 * 2);
}

WaybackPlugin::~WaybackPlugin() {
  if (curl) {
    curl_easy_cleanup(curl);
    curl = nullptr;
  }
}

bool WaybackPlugin::configure(char option, const std::string value) {
  switch (option) {
  case 't':
    cutoff = parseTime(value);
    break;
  default:
    return false;
  }

  return true;
}

static size_t wayback_curl_callback(char *contents, size_t size, size_t nmemb, void *userdata) {
  size_t realsize = size * nmemb;
  WaybackPlugin *plugin = (WaybackPlugin*)userdata;
  plugin->response += std::string(contents, realsize);
  return realsize;
}

bool WaybackPlugin::checkDomain(const std::string domain) {
  std::string requestTimestamp = "20060101";
  std::string url =
    "https://archive.org/wayback/available?url=" +
    domain +
    "&timestamp=" +
    requestTimestamp;

  response = "";
  curl_easy_reset(curl);
  curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
  curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
  curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, wayback_curl_callback);
  curl_easy_setopt(curl, CURLOPT_WRITEDATA, this);
  auto error = curl_easy_perform(curl);

  if (error != CURLE_OK) {
    throw std::runtime_error("Unable to query archive.org API " + std::string(curl_easy_strerror(error)));
  }

  auto json = nlohmann::json::parse(response);
  response = "";

  if (!json.is_object()) {
    return false;
  }

  auto snapshots = json.find("archived_snapshots");

  if (snapshots == json.end()) {
    return false;
  }

  auto closest = (*snapshots).find("closest");

  if (closest == (*snapshots).end()) {
    return false;
  }

  auto available = (*closest).find("available");

  if (available == (*closest).end()) {
    return false;
  }

  if (!(*available)) {
    return false;
  }

  auto timestamp = (*closest).find("timestamp");

  if (timestamp == (*closest).end()) {
    return false;
  }

  return parseTime(*timestamp) < cutoff;
}

time_t WaybackPlugin::parseTime(const std::string timestamp) const {
  if (timestamp.length() < 8) {
    throw std::runtime_error("Invalid Archive.org timestamp");
  }

  struct tm timeinfo = {};
  timeinfo.tm_year = std::stoi(timestamp.substr(0, 4)) - 1900;
  timeinfo.tm_mon = std::stoi(timestamp.substr(4, 2));
  timeinfo.tm_mday = std::stoi(timestamp.substr(6, 2));
  return mktime(&timeinfo);
}
